/* eslint-disable @typescript-eslint/no-var-requires */
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: "./src/index.tsx",
  mode: "development",
  devServer: {
    contentBase: "./dist",
    hot: true,
  },
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist"),
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: "ts-loader",
      },
    ],
  },
  resolve: { extensions: [".ts", ".tsx", ".js"] },
  plugins: [
    new HtmlWebpackPlugin({
      templateContent: `
    <html>
      <body>
        <div id="root" />
      </body>
    </html>
  `,
    }),
  ],
};
