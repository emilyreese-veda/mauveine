import { theme as chakraTheme, extendTheme } from "@chakra-ui/react";

export const mauveineTheme = extendTheme({
  colors: {
    brand: chakraTheme.colors.blue,
  },
  styles: {
    global: {
      body: {
        bg: "gray.200",
        color: "purple",
      },
    },
  },
});
