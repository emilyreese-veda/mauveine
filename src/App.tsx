import * as React from "react";
import { ChakraProvider } from "@chakra-ui/react";
import Demo from "./Demo";
import { mauveineTheme } from "./theme";

const App: React.FC = () => (
  <ChakraProvider theme={mauveineTheme}>
    <Demo />
  </ChakraProvider>
);

export default App;
